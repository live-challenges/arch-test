import json
import sqlite3
import logging
import asyncio
import aiohttp
import requests
from datetime import datetime
from aiosseclient import aiosseclient

DB = "wikimoney.db"
#COPY after deploy the API, why does it changes?
CLIENT_URL = "http://localhost:4566/restapis/otlf7smob7/local/_user_request_"

def create_db():
    try:
        con = sqlite3.connect(DB)
        cursorObj = con.cursor()
        cursorObj.execute("""CREATE TABLE wiki_updates(id integer PRIMARY KEY AUTOINCREMENT
            , change text, user text, calculations text, date_created timestamp)""")
        con.commit()
    except Exception as e:
        print(e)

async def log(event):
    print("Event received from user: %s" % json.loads(event.data)['user'])

async def process(event):
    try:
        data = json.loads(event.data)
        con = sqlite3.connect(DB)
        cursorObj = con.cursor()
        now = datetime.now()
        query = """INSERT INTO wiki_updates (change, user, calculations, date_created) 
                VALUES (?, ?, ?, ?)"""
        myobj = {'title': data["user"]}
        if data["bot"] == True:    
            x = requests.post(CLIENT_URL+"/fib", json = myobj)
        else:
            x = requests.post(CLIENT_URL+"/fac", json = myobj)
        calc = (json.loads(x.text))['res']
        cursorObj.execute(query,(json.dumps(data), data['user'], calc, now,))
        con.commit()
    except Exception as e:
        print(e)
    finally:
        con.close()

async def main():
    async for event in aiosseclient('https://stream.wikimedia.org/v2/stream/recentchange'):
        await log(event)
        #Looks like we should be processing this faster, when commented the speed is reaaally fast, why?
        await process(event)

create_db()
loop = asyncio.get_event_loop()
loop.run_until_complete(main())