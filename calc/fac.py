import json 
import time
import logging

#dynamodb client
def dyna_saver(req, res):
    dynamodb_endpoint = 'http://%s:4566' % os.environ['LOCALSTACK_HOSTNAME']
    dynamodb = boto3.resource('dynamodb', endpoint_url=dynamodb_endpoint)
    table = dynamodb.Table(os.environ['DYNAMODB_TABLE'])
    timestamp = str(time.time())
    item = {
        'id': str(uuid.uuid1()),
        'request': req,
        'response': res,
        'createdAt': timestamp,
        'updatedAt': timestamp,
    }

    # write the todo to the database
    table.put_item(Item=item)

def factorial(n):
    if n <= 1:
        return n
    else:
        return n * factorial(n-1)

def calc(event, context):
    n = len(event["body"])
    res = factorial(n)
    return {
        "statusCode":200
        ,"body": json.dumps({'n':n,'res':f"{res}"})
    }