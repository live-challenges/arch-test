import json

def fibonacci(n):
    if n <= 1:
        return n
    else:
       return(fibonacci(n-1) + fibonacci(n-2))

def calc(event, context):
    n = len(event["body"])
    res = fibonacci(n)
    return {
        "statusCode":200
        ,"body": json.dumps({'n':n,'res':f"{res}"})
    }