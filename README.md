# Wiki Data Stream Processor

This project is a tiny sample live coding challenge that includes a fictional situation that sets a business use case with:

- A customer perspective of the situation
- Code to ingest data from a public stream source
- An implmentation of a Serverless HTTP API
- A Local RDBMS storage (sqlite3)
- A workflow to create an end-user report

Please read this file completely in loud voice before start doing anything.

## Running instructions :thumbsup:

- Clone this repo
- install python 3
- install pip
- install requirements.txt
- install localstack
- install serverless
- install serverless-localstack plugin
- run the local deployer
    - ./deploy.sh
- copy the **endpoint:** of the deployed API
- replace the **CLIENT_URL** in the [SSE-Client](./sse-client/wikiclient.py)
- run the sse-client
    - python sse-client/wikiclient.py

## Context 

Our company (WikiMilkersCorp) has signed a contract :smile: to analyze the bots/humans contributions to the Wiki foundation, we have to listen to every change made to any of the Wikis and make a complicated calculation if the change was made by a bot or a human, every month we have to deliver a report with the number of changes made by every group (bots/humans) to our customer who will pay for the calculations made.

The following diagram shows the product design:

```mermaid
graph LR;
    Wikimedia-->Wikiclient.py;
    Wikiclient.py<-->Calculations;
    Wikiclient.py-->Database;
    Database-->Csv;
    Csv-->Report
    Report-->Client;
```

You can see a sample report opening the [Report Template](./report-template.ods) of this project.

### Software developed

Our solution has been written twice, first version was a monolith developed in groovy, but the software developer left the company and we hired a senior developer who rewrote it completely in python, also a microservices architecture was adopted with serverless to speed up the things. The senior developr left the company after the current solution was deployed to production. Some future improvements were partially included in one of the endpoints, but, there is no documentation about it. 

The calculations are done in a HTTP API with two endpoints implemented as lambda functions [Fib](./calc/fib.py) and [Fac](./calc/fac.py) that can scale infinitely (???), the data to build the final report is stored in a local sqlite3 instance.

### C-suite concerns :worried:

- We think that we are leaving money in the table, the [Test](./sse-client/wikitester.py) shows that there is a large number of wiki modifications per minute, but we don't process as many even with the microservices approach.
- We think that the propietary calculations are unsecure
- We think that our backup strategy (copy the database to an S3 bucket) is not optimal
- We are afraid that the solution is not resilient
- We are afraid that the solution is not fault tolerant
- We are afraid that the solution is not highly available
- We don't understand how the solution is built or what changes must be made
- We think that the CI/CD capabilities are not fully used
- We are not using the Static Application Security Testing provided by our CI/CD platform

### Customer concerns :sweat:

- We think that you are not detecting correctly the number of bots/humans in the [report](./report-template.ods)?
- Are you using the [event payload](./sample-event-payload.json) sample?
- We think that you are not processing all the data that we expose
- The report looks clunky
- We don't understand how the solution is built or what changes must be made

### Operations concerns :weary:

- The workflow to create the monthly report is insanely cumbersome:
    - Run the [Data extractor](./rep-client/wikicsv.py)
    - Import the [Csv file](./output.csv) into the report template
    - Update the report template to generate the chart
    - Cleanup the data table in the database

- If we don't cleanup the database eventually the system stops working because there is too much data
- We don't understand how the solution is built or what changes must be made

## Expectations :grinning:

The main expectation that we have is that you LEAD the conversation about how to tackle this challenge.
As an architect we hope that you can address the concerns of the project's stakeholders. It means that you:

- Run the current project
- Write the project´s improvement oppportunities 
- Prioritize the improvements
- Make at least one (1) improvement
    - Create a MR, please write it well
- Create some high level technical documentation 

You decide the order of the tasks, format, standards, tools, products, team, etc. that you will use. 
